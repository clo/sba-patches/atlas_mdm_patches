From 35d4f14c12e32f956253c319a51c92e525e2f490 Mon Sep 17 00:00:00 2001
From: Stephanie Buser <sbuser@codeaurora.org>
Date: Wed, 9 Nov 2016 17:49:52 -0800
Subject: [PATCH 10/22] usb:gadget: Resolving USB Disconnect issues

On USB plug out, we were not cleaning up correctly due
to read workqueue always having a pending read work from
USB IPC XPRT layer.
Now, upon USB disconnect we set is_exiting flag and force
read workqueue to exit, this enables a clean disconnect.

Change-Id: I9c11fbdb19a784a0e0dea0d83c70507478ca11e1
Signed-off-by: Pranav Desai <pranavd@qti.qualcomm.com>
---
 drivers/usb/gadget/function/f_ipcrtr.c | 27 ++++++++++++++++++++++++---
 drivers/usb/gadget/function/f_ipcrtr.h |  2 ++
 2 files changed, 26 insertions(+), 3 deletions(-)

diff --git a/drivers/usb/gadget/function/f_ipcrtr.c b/drivers/usb/gadget/function/f_ipcrtr.c
index 5489540..ed36214 100644
--- a/drivers/usb/gadget/function/f_ipcrtr.c
+++ b/drivers/usb/gadget/function/f_ipcrtr.c
@@ -367,11 +367,13 @@ static int ipcrtr_ctrl_read(char *buf, unsigned int count)
 		log_event_dbg("Requests list is empty. Wait. \n");
 		spin_unlock_irqrestore(&ipcrtr->lock, flags);
 		ret = wait_event_interruptible(ipcrtr->read_wq,
-			!list_empty(&ipcrtr->cpkt_req_q));
+				!list_empty(&ipcrtr->cpkt_req_q));
 		if (ret < 0) {
 			log_event_err("Waiting failed \n");
 			return -ERESTARTSYS;
 		}
+		if (ipcrtr->is_exiting)
+			return -ESHUTDOWN;
 		log_event_dbg("Received request packet \n");
 		spin_lock_irqsave(&ipcrtr->lock, flags);
 	}
@@ -804,6 +806,7 @@ static int ipcrtr_set_alt(struct usb_function *f, unsigned intf, unsigned alt)
 			goto fail;
 		}
 		ipcrtr->notify->driver_data = ipcrtr;
+		ipcrtr->is_exiting = false;
 		log_event_dbg(" Control active intf=%u, alt=%u \n", intf, alt);
 	} else {
 		log_event_err("usb ep#%s wrong interface activated, inf#%d \n",
@@ -840,6 +843,8 @@ static void usb_xprt_init(struct work_struct *work)
 		return;
 	}
 
+	/* Wake up read queues to ensure all pending packets have been read */
+
 	/* Register with USB IPC XPRT layer */
 	ret = msm_ipc_router_usb_ipc_xprt_register(&xprt_ops);
 	if (ret) {
@@ -881,6 +886,8 @@ static void usb_xprt_deinit(struct work_struct *work)
 		return;
 	}
 
+	ipcrtr_ctrl_clear_cpkt_queues(ipcrtr, false);
+
 	log_event_dbg("usb ep#%s, XPRT De-registration success \n",
 					ipcrtr->notify->name);
 }
@@ -894,9 +901,11 @@ static void ipcrtr_disable(struct usb_function *f)
 {
 	struct f_ipcrtr *ipcrtr = func_to_ipcrtr(f);
 	int ret;
+	struct ipcrtr_ctrl_pkt *cpkt;
 
 	atomic_set(&ipcrtr->connected, 0);
 
+	log_event_err("%s: Enter %s", __func__, f->name);
 
 	 /* Disable Control Path */
 	if (ipcrtr->notify &&
@@ -908,9 +917,21 @@ static void ipcrtr_disable(struct usb_function *f)
 
 	atomic_set(&ipcrtr->notify_count, 0);
 
-	ipcrtr_ctrl_clear_cpkt_queues(ipcrtr, false);
+	/* Add a fake packet at the end */
+	cpkt = ipcrtr_ctrl_pkt_alloc(IPCRTR_FAKE_PKT_LEN, GFP_ATOMIC);
+	if (IS_ERR(cpkt)) {
+		log_event_err("%s: func pkt allocation failed \n", __func__);
+		return -ENOMEM;
+	}
+
+	list_add_tail(&cpkt->list, &ipcrtr->cpkt_req_q);
+	ipcrtr->is_exiting = true;
+	/* WakeUp Read queues to ensure that read callbacks are released */
+	wake_up(&ipcrtr->read_wq);
+	log_event_err("%s: After waking up work queue", __func__);
 
-	queue_delayed_work(ipcrtr->ipcrtr_wq, &ipcrtr->exit_work, 0);
+	queue_delayed_work(ipcrtr->ipcrtr_wq, &ipcrtr->exit_work,
+						msecs_to_jiffies(100));
 }
 
 /**
diff --git a/drivers/usb/gadget/function/f_ipcrtr.h b/drivers/usb/gadget/function/f_ipcrtr.h
index aeb182d..da93148 100644
--- a/drivers/usb/gadget/function/f_ipcrtr.h
+++ b/drivers/usb/gadget/function/f_ipcrtr.h
@@ -15,6 +15,7 @@
 #define _F_IPCRTR_H
 
 #define IPCRTR_CTRL_NAME_LEN	40
+#define	IPCRTR_FAKE_PKT_LEN	100
 
 #define IPC_USB_XPRT_MAX_READ_SZ	(4 * 1024)
 #define IPC_USB_XPRT_MAX_WRITE_SZ	(4 * 1024)
@@ -114,6 +115,7 @@ struct f_ipcrtr {
 
 	spinlock_t lock;
 	bool is_open;
+	bool is_exiting;
 
 	struct workqueue_struct *ipcrtr_wq;
 	struct delayed_work init_work;
-- 
1.8.2.1

