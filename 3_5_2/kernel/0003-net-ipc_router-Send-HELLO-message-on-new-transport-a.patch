From d64d1e1e4812a7f6e31303063bdd7aa7a68cd333 Mon Sep 17 00:00:00 2001
From: Karthikeyan Ramasubramanian <kramasub@codeaurora.org>
Date: Fri, 21 Oct 2016 16:43:22 -0600
Subject: [PATCH 03/22] net: ipc_router: Send HELLO message on new transport
 addition

Hello message is currently sent in response to a hello message received
over a transport. When two IPC Routers operate in this slave model,
then neither of them exchange the Hello message. This will halt further
communication from happening.

Update the IPC Router to send a hello message once a new transport is
added.

Change-Id: I968961560924e4422ca1a86625ae4d85bac7059d
Signed-off-by: Karthikeyan Ramasubramanian <kramasub@codeaurora.org>
---
 net/ipc_router/ipc_router_core.c | 49 ++++++++++++++++++++++++++++------------
 1 file changed, 35 insertions(+), 14 deletions(-)

diff --git a/net/ipc_router/ipc_router_core.c b/net/ipc_router/ipc_router_core.c
index fae4158..9b3e96d 100644
--- a/net/ipc_router/ipc_router_core.c
+++ b/net/ipc_router/ipc_router_core.c
@@ -149,6 +149,7 @@ struct msm_ipc_router_xprt_info {
 	void *log_ctx;
 	struct kref ref;
 	struct completion ref_complete;
+	bool hello_sent;
 };
 
 #define RT_HASH_SIZE 4
@@ -2464,12 +2465,41 @@ static void do_version_negotiation(struct msm_ipc_router_xprt_info *xprt_info,
 	}
 }
 
+/**
+ * do_send_hello_msg() - Send a hello message
+ * @xprt_info: XPRT over which the Hello message has to be sent.
+ *
+ * Return: 0 on success, standard Linux error codes on failure.
+ */
+static int do_send_hello_msg(struct msm_ipc_router_xprt_info *xprt_info)
+{
+	union rr_control_msg ctl;
+	int rc = 0;
+
+	if (xprt_info->hello_sent)
+		return 0;
+
+	memset(&ctl, 0, sizeof(ctl));
+	ctl.hello.cmd = IPC_ROUTER_CTRL_CMD_HELLO;
+	ctl.hello.checksum = IPC_ROUTER_HELLO_MAGIC;
+	ctl.hello.versions = (uint32_t)IPC_ROUTER_VER_BITMASK;
+	ctl.hello.checksum = ipc_router_calc_checksum(&ctl);
+	rc = ipc_router_send_ctl_msg(xprt_info, &ctl,
+				     IPC_ROUTER_DUMMY_DEST_NODE);
+	if (rc < 0) {
+		IPC_RTR_ERR("%s: Error sending reply HELLO message\n",
+								__func__);
+		return rc;
+	}
+	xprt_info->hello_sent = true;
+	return 0;
+}
+
 static int process_hello_msg(struct msm_ipc_router_xprt_info *xprt_info,
 				union rr_control_msg *msg,
 				struct rr_header_v1 *hdr)
 {
 	int i, rc = 0;
-	union rr_control_msg ctl;
 	struct msm_ipc_routing_table_entry *rt_entry;
 
 	if (!hdr)
@@ -2484,19 +2514,7 @@ static int process_hello_msg(struct msm_ipc_router_xprt_info *xprt_info,
 	kref_put(&rt_entry->ref, ipc_router_release_rtentry);
 
 	do_version_negotiation(xprt_info, msg);
-	/* Send a reply HELLO message */
-	memset(&ctl, 0, sizeof(ctl));
-	ctl.hello.cmd = IPC_ROUTER_CTRL_CMD_HELLO;
-	ctl.hello.checksum = IPC_ROUTER_HELLO_MAGIC;
-	ctl.hello.versions = (uint32_t)IPC_ROUTER_VER_BITMASK;
-	ctl.hello.checksum = ipc_router_calc_checksum(&ctl);
-	rc = ipc_router_send_ctl_msg(xprt_info, &ctl,
-				     IPC_ROUTER_DUMMY_DEST_NODE);
-	if (rc < 0) {
-		IPC_RTR_ERR("%s: Error sending reply HELLO message\n",
-								__func__);
-		return rc;
-	}
+	do_send_hello_msg(xprt_info);
 	xprt_info->initialized = 1;
 
 	/* Send list of servers from the local node and from nodes
@@ -4080,6 +4098,9 @@ static int msm_ipc_router_add_xprt(struct msm_ipc_router_xprt *xprt)
 	list_add_tail(&xprt_info->list, &xprt_info_list);
 	up_write(&xprt_info_list_lock_lha5);
 
+	xprt_info->hello_sent = false;
+	do_send_hello_msg(xprt_info);
+
 	down_write(&routing_table_lock_lha3);
 	if (!routing_table_inited) {
 		init_routing_table();
-- 
1.8.2.1

